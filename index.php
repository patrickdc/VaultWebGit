<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vault</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" media="screen" href="style/style.css" />
</head>

<body>
  <div id="MainDivPvlt">
  <div id="VaultJSPvlt" class="VaultDivPvlt">
        <img src="images/vaultclosed.jpg" id="VaultImgPvlt">
      </div>
    <div id="MainContentPvlt">
      <div id="MonitorPvlt">
        <h3 id="MainHeaderMonitorPvlt"></h1>
        <h3 id="CounterMonitorPvlt"></h3>
      </div>
      <div id="InputDivPvlt">
        <div id="ButtonsDivPvlt">
          <button class="ButtonPvlt" id="ButtonOnePvlt" onclick="buttonClick(1)">1</button>
          <button class="ButtonPvlt" id="ButtonTwoPvlt" onclick="buttonClick(2)">2</button>
          <button class="ButtonPvlt" id="ButtonThreePvlt" onclick="buttonClick(3)">3</button>
        </div>
        <div id="ColorDivPvlt">
          <div id="GreenDivPvlt"></div>
          <div id="RedDivPvlt"></div>
        </div>
      </div>
    </div>
  </div>
</body>

<script>
  var vaultCodePvlt = "121";
  var guessedCodePvlt = "";
  var correctAttemptsPvlt = 0;
  var wrongAttemptsPvlt = 0;
  var buttonsPressedPvlt = 0;
  var openVault = false;
  document.getElementById("MainHeaderMonitorPvlt").innerHTML = " ";
  updateAttempts();

  function buttonClick(buttonIDPvlt) 
  {
    buttonsPressedPvlt++;
    guessedCodePvlt = guessedCodePvlt + buttonIDPvlt;
    document.getElementById("MainHeaderMonitorPvlt").innerHTML = guessedCodePvlt;
    updateAttempts();
    
    if (buttonsPressedPvlt < 3 && openVault == true)
    {
      document.getElementById("VaultImgFadePvlt").id = "VaultImgPvlt";
      openVault = false;
      return false; 
    }

    if (buttonsPressedPvlt == 3) 
    {
      if (guessedCodePvlt == vaultCodePvlt) 
      {
        correctAttemptsPvlt++;
        activeBlockPvlt = "GreenDivPvlt";
        var openSound = new Audio('sounds/correct.mp3');
        openSound.loop = false;
        openSound.play();
        document.getElementById("MainHeaderMonitorPvlt").innerHTML = "The code is correct! Unlocking..";
        BlinkFunction();
        FadeImgVault();
        updateAttempts();
        alert("The code is correct! Unlocking.");
      } 
      else 
      {
        wrongAttemptsPvlt++;
        activeBlockPvlt = "RedDivPvlt";
        var closedSound = new Audio('sounds/wrong.mp3');
        closedSound.loop = false;
        closedSound.play();
        document.getElementById("MainHeaderMonitorPvlt").innerHTML = "The code is incorrect! Resetting..";
        updateAttempts();
        BlinkFunction();
        alert("The code is incorrect. Resetting.");
      }
      LockControls();
      buttonsPressedPvlt = 0;
      guessedCodePvlt = "";
    }
  }

  function LockControls()
  {
    var controlTimer=0;
    document.getElementById("ButtonOnePvlt").disabled = true;
    document.getElementById("ButtonTwoPvlt").disabled = true;
    document.getElementById("ButtonThreePvlt").disabled = true;

    var controlslock = setInterval(function () 
      {		
        controlTimer++;
        if (controlTimer==5) 
        {
          document.getElementById("ButtonOnePvlt").disabled = false;
          document.getElementById("ButtonTwoPvlt").disabled = false;
          document.getElementById("ButtonThreePvlt").disabled = false;
          controlTimer = 0;
          clearInterval(controlslock);
        } 
      }, 1000);
  }


  function BlinkFunction() 
  {
    var divBlockPvlt = document.getElementById(activeBlockPvlt);
    //counter start at zero
    var intervalTimer = 0;
    var blink = setInterval(function () {
      //add +1 every time the setinterval function runs		
      intervalTimer++;
      //method to show div on and off
      //change the css of the green and red box to create a blinking effect
      if (divBlockPvlt.style.visibility == 'hidden') {
        divBlockPvlt.style.visibility = 'visible';
      } else {
        divBlockPvlt.style.visibility = 'hidden';
      }
      //check if the interval has runned ten times
      if (intervalTimer == 6) {
        //ClearInterval function stops the interval after 10 times
        clearInterval(blink);
      }
    }, 250);
  }

  function FadeImgVault()
  {
    openVault = true;
    document.getElementById("VaultImgPvlt").id = "VaultImgFadePvlt"; 
    return false;
  }

  function updateAttempts()
  {
    document.getElementById("CounterMonitorPvlt").innerHTML = "Correct attempts: " + correctAttemptsPvlt + "<br>" + "Wrong attempts: " + wrongAttemptsPvlt;
  }
</script>
</html>